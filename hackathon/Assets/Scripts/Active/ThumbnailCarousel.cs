using UnityEngine;
using System.Collections;
using System.Collections.Generic;


/*
 * 
 * NOTE: 
 * Pooling using trigger with colliders wont work with current rotation implementation because
 * wheel may rotate so much during one frame that spawner goes through trigger without triggering it.
 * This is due the carousel is rotated instantly to spesific angle with transform.Rotate() ,
 * instead it could being rotated with forces & rigidbody (at that case collision would be detected)
 * 
 */

public class ThumbnailCarousel: MonoBehaviour {
	
	//Use this value to read which thumbnail is selected. Value is -1 if carousel is currently rotating.
	public int selectedThumbnailIndex;
	
	
	/*********ASSIGN THESE OBJECTS VIA INSPECTOR***********/
	//Carousels pivot point which carousel is rotated around
	public Transform carouselPivot;
	//Scroll trigger which mouse is raycasted against
	public Transform scrollTrigger;
	//Sensor which detects right thumbnail index
	public Transform sensor;
	//Spawnpoints with lower z-axis position than border will be visible, good for graphical debugging
	public Transform visibleBorder;
	//Instantiated thumbnail's parent
	public Transform thumbnailPoolObject;
	//Parent object of generated spawnpoints
	public Transform holderParent;
	//Pool parent for attached objects
	public Transform attachedParent;
	//If camera is not assigned the main camera is used
	public Camera cam;
	//Set vehicle prefabs to this list
	public List<Texture2D> thumbnails;
	
	
	/*********ASSIGN THESE VALUES VIA INSPECTOR***********/
	//How fast the carousel is rotated due to mouse movement
	public float rotationMultiplier = 0.3f;
	//Threshold to detect one click/tap
	public float tapTimeThreshold;
	//Determines how big the carousel is
	public float CAROUSELRADIUS;
	//Determines the density of thumbnails
	public int HOLDERDAMOUNT=20;
	//Thumbnails transforms x,y-size
	public Vector2 thumbnailSize;
	//If true this is horizontal carousel
	public bool horizontal;

	
	

	
	

	//If false, main loop at update is not executed
	public bool enabled;
	//Like enabled but switching value is done at the end of the frame
	public bool hidden=false;
	//If true, rotation of carousel is enabled
	public bool rotationEnabled=true;
	//private array of these spawnpoints
	private ThumbnailHolder[] holders;	
	//Outer list contains per vehicle lists of pooled thumbnails
	private List<Thumbnail> thumbnailPool;
	//Linked list of  thumbnail indexes
	private LinkedList<int> linkedIndexList;
	//generated list of materials
	private List<Material> materials;
	//List of current spawnpoints which have thumbnail
	private List<ThumbnailHolder> visibleHolders;
	//Initial position of carouselPivot
	private Vector3 carouselPosition;
	//Mouse position in last frame
	private Vector3 prevMousePos;
	//Mouse/touch duration
	private float mouseHoldTime=0;
	//Is rotation continuing
	private bool rotationActive = false;	
	//last selected index used when rotating back to last selected thumbnail
	private int lastIndex=-1;
	//True for one frame after selected thumbnail is clicked
	private bool click=false;
	
	
	//ATTACH OTHER STUFF TO THUMBNAIL FEATURE
	private List<List<Transform>> attachedObjects;
	private List<List<Vector3>> attachedSize;
	private List<List<Vector3>> attachedOffset;	
	private List<Thumbnail> allThumbnails;
	
	
	// Use this for initialization
	void Awake () {
		if(cam==null){
			cam=GameObject.FindGameObjectWithTag("MainCamera").camera;
		}
		
		//Init spawnpoint list and spawnpoint locations
		holders= new ThumbnailHolder[HOLDERDAMOUNT];
		//wheelItems= new ScrollWheelItem[HOLDERDAMOUNT];
		for(int i=0;i<HOLDERDAMOUNT;i++){
			Transform holderTrans = new GameObject().transform;
			holderTrans = new GameObject().transform;
			holderTrans.name=i.ToString();
			holderTrans.parent=holderParent;
			holderTrans.localPosition=Vector3.zero;
			holderTrans.eulerAngles=new Vector3(i*(360/HOLDERDAMOUNT)+180,0,0);
			holderTrans.Translate(Vector3.forward *CAROUSELRADIUS);
			BoxCollider col = holderTrans.gameObject.AddComponent<BoxCollider>();
			col.size = new Vector3(thumbnailSize.x,thumbnailSize.y,0.001f);
			ThumbnailHolder t= new ThumbnailHolder(holderTrans,null);
			holders[i]=t;
			//add ScrollWheelitem component
			//wheelItems[i]= spawnPoints[i].gameObject.AddComponent<ScrollWheelItem>();
		}
		//Init transform positions
		carouselPosition=carouselPivot.position;
		sensor.position=carouselPosition;
		sensor.Translate(-carouselPivot.forward*CAROUSELRADIUS);
		//scrollTrigger.position=carouselPosition;
		//scrollTrigger.Translate(-carouselPivot.forward*(CAROUSELRADIUS+1));
		
		//Init lists of size thumbnails.Count		
		materials=new List<Material>();
		attachedObjects=new List<List<Transform>>();
		attachedOffset=new List<List<Vector3>>();
		attachedSize=new List<List<Vector3>>();
		for(int i=0;i<thumbnails.Count;i++){
			//Material list
			Material temp = new Material(Shader.Find("Unlit/Transparent Cutout"));
			materials.Add(temp);
			temp.mainTexture= thumbnails[i];
			temp.SetTextureScale("_MainTex",-Vector2.one);
			//Attached objects
			attachedObjects.Add(new List<Transform>());
			attachedOffset.Add(new List<Vector3>());
			attachedSize.Add(new List<Vector3>());
		}
		
		
		
		allThumbnails=new List<Thumbnail>();
		
		
		visibleHolders= new List<ThumbnailHolder>();	
		prevMousePos=Vector3.zero;
		
		//Init linked list of indexes.
		linkedIndexList=new LinkedList<int>();
		linkedIndexList.AddFirst(0);
		for(int i=1;i<thumbnails.Count;i++){
			linkedIndexList.AddLast(i);
		}
	
		initializePool();
		updateVisibleThumbnails();
		updateSelectedThumbnailIndex();
		
	
		
		if(horizontal){
			carouselPivot.Rotate(Vector3.forward,90);
			scrollTrigger.Rotate(Vector3.forward,90);
		}
		
		if(CAROUSELRADIUS<=0){
			CAROUSELRADIUS=20;
			Debug.Log("Invalid carousel radius, used default value:20");
		}
		
	}
	
	// Update is called once per frame
	void Update () {
		if(enabled){
			click=false;
			//Wheel rotation
			//if(rotationEnabled){
				if(Input.GetMouseButton(0) && rotationEnabled){
					mouseHoldTime+=Time.deltaTime;
					selectedThumbnailIndex=-1;
					stopTweening();
					Ray ray = cam.ScreenPointToRay(Input.mousePosition);
					RaycastHit hit;
					//Detect mouse movement withc raycasting
					if (!rotationActive && Physics.Raycast(ray, out hit,  200.0f)) { // Casts the ray and get the first game object hit
						if(hit.transform.gameObject==scrollTrigger.gameObject) {
							rotationActive=true;
						}
					}
					//Apply rotation
					if (rotationActive && rotationEnabled){
						if (prevMousePos!=Vector3.zero){						
							if(horizontal){
								float amount = (prevMousePos.x-Input.mousePosition.x)*rotationMultiplier;
								carouselPivot.RotateAround(carouselPosition,Vector3.up,amount);
							}
							else{
								float amount = (prevMousePos.y-Input.mousePosition.y)*-rotationMultiplier;
								carouselPivot.RotateAround(carouselPosition,Vector3.right,amount);
							}
						}
						prevMousePos=Input.mousePosition;
					}
				}
			//}
			//Mouse released-> tween carousel to valid angle
			else if (Input.GetMouseButtonUp(0)) {
				//print ("TWEEEN!!");
				tweenToNearestValidAngle();
				rotationActive=false;
				prevMousePos=Vector3.zero;
				
				//DETECT IF MIDDLE THUMBNAIL WAS CLICKED
				//set scroll trigger to ignore raycast layer temporarily so we can raycast against thumbnails
				scrollTrigger.gameObject.layer=2;
				Ray ray = cam.ScreenPointToRay(Input.mousePosition);
				RaycastHit hit;
				//Detect mouse movement withc raycasting
				if (mouseHoldTime<tapTimeThreshold && Physics.Raycast(ray, out hit,  200.0f) && hit.transform.parent==holderParent) { // Casts the ray and get the first game object hit
					foreach(ThumbnailHolder h in holders){
						if(hit.transform==h.holder && Vector3.Distance(sensor.position,h.holder.position)<1f){
							updateSelectedThumbnailIndex();
							click=true;
							//print ("clicked thumb"+h.holder.name+"selected"+selectedThumbnailIndex);
						}
					}
				}
				//set scrolltrigger back to default layer
				scrollTrigger.gameObject.layer=0;
				
				mouseHoldTime=0;
			}
			updateVisibleThumbnails();
		}
	}

	public void show(){
		enabled=true;
		updateVisibleThumbnails();
		StartCoroutine(setHiddenAtEndOfFrame(false));
		centerToThumbnail(lastIndex);
		updateSelectedThumbnailIndex();
	}
	
	public void hide(){
		enabled=false;
		iTween.Stop(carouselPivot.gameObject);
		StartCoroutine(setHiddenAtEndOfFrame(true));
		updateVisibleThumbnails();
		updateSelectedThumbnailIndex();
		prevMousePos=Vector3.zero;
		click=false;
		foreach(ThumbnailHolder h in holders){
			if(visibleHolders.Contains(h)) holderExitedVisible(h);
		}
		//enabled=false;
		
	}
	
	//updates thumbnails so that holders with z-position<visibleborder.z gets hidden and vice versa
	void updateVisibleThumbnails(){
		//Show thumbnail if spawner over z-axis threshold
		for(int i=0;i<holders.Length;i++){
			if(holders[i].holder.position.z<visibleBorder.position.z && !visibleHolders.Contains(holders[i])){
				holderEnteredVisible(holders[i]);
			}
			else if(holders[i].holder.position.z>visibleBorder.position.z && visibleHolders.Contains(holders[i])){
				holderExitedVisible(holders[i]);
			}
		}
	}
	
	//Tween to nearest valid angle after
	void tweenToNearestValidAngle(){
		selectedThumbnailIndex=-1;
		if(horizontal){
			float nearestAngle= ((Mathf.Round(carouselPivot.localRotation.eulerAngles.y/(360/HOLDERDAMOUNT)))*(360/HOLDERDAMOUNT));
			float angleDifference=nearestAngle-carouselPivot.localRotation.eulerAngles.y;
			float tweenTime=Mathf.Abs(angleDifference)/18f;
			iTween.RotateTo(carouselPivot.gameObject,iTween.Hash("rotation",new Vector3(carouselPivot.localRotation.eulerAngles.x,nearestAngle,carouselPivot.localRotation.eulerAngles.z),
			"time",tweenTime,"islocal",true,"easetype","easeOutCubic","ignoretimescale",true,"oncompletetarget",gameObject,"oncomplete","updateSelectedThumbnailIndex"));
		}
		
		else{
			float nearestAngle= ((Mathf.Round(carouselPivot.localRotation.eulerAngles.x/(360/HOLDERDAMOUNT)))*(360/HOLDERDAMOUNT));
			float angleDifference=nearestAngle-carouselPivot.localRotation.eulerAngles.x;
			float tweenTime=Mathf.Abs(angleDifference)/18f;
			iTween.RotateTo(carouselPivot.gameObject,iTween.Hash("rotation",new Vector3(nearestAngle,carouselPivot.localRotation.eulerAngles.y,carouselPivot.localRotation.eulerAngles.z),
			"time",tweenTime,"islocal",true,"easetype","easeOutCubic","ignoretimescale",true,"oncompletetarget",gameObject,"oncomplete","updateSelectedThumbnailIndex"));
	
		}
		
	}
	
	//Updates selectedThumbnailIndex  to index of closest thumbnail to the sensor
	void updateSelectedThumbnailIndex(){
		//which thumbnail is selected
		int indx=-1;
		float minDistance=Vector3.Distance(sensor.position,holders[0].holder.position);
		ThumbnailHolder smallest=holders[0];
		foreach(ThumbnailHolder h in holders){
			if(Vector3.Distance(sensor.position,h.holder.position)<minDistance){ smallest=h; minDistance=Vector3.Distance(sensor.position,h.holder.position);}
		}
		
		if(smallest!=null && smallest.thumbnail!=null && smallest.thumbnail.index!=-1){
			changeSelectedIndex(smallest.thumbnail.index);
		}
		else Debug.Log("couldnt determine right index");
	}
	
	//Stops tweening in case tweening is interrupted
	void stopTweening(){
		if(carouselPivot.GetComponent<iTween>()!=null){
			Destroy(carouselPivot.GetComponent<iTween>());
		}
	}
	
	//Rotate instantly to next valid angle	
	void rotateToNext(){
		if(horizontal){
			float nextAngle = ((Mathf.Round(carouselPivot.localEulerAngles.y/(360/HOLDERDAMOUNT)))*(360/HOLDERDAMOUNT)+(360/HOLDERDAMOUNT));
			float amount=carouselPivot.eulerAngles.y-nextAngle;
			carouselPivot.localEulerAngles=new Vector3(carouselPivot.localEulerAngles.x,nextAngle,carouselPivot.localEulerAngles.z);
			//carouselPivot.Rotate(carouselPivot.forward,amount);
		}
		/*if(horizontal){
			float nearestAngle= ((Mathf.Round(carouselPivot.localRotation.eulerAngles.y/(360/HOLDERDAMOUNT)))*(360/HOLDERDAMOUNT));
			float angleDifference=nearestAngle-carouselPivot.localRotation.eulerAngles.y;
			float tweenTime=Mathf.Abs(angleDifference)/18f;
			iTween.RotateTo(carouselPivot.gameObject,iTween.Hash("rotation",new Vector3(carouselPivot.localRotation.eulerAngles.x,nearestAngle,carouselPivot.localRotation.eulerAngles.z),
			"time",tweenTime,"islocal",true,"easetype","easeOutCubic","ignoretimescale",true,"oncompletetarget",gameObject,"oncomplete","updateSelectedThumbnailIndex"));
		}*/
		else{
			float nextAngle = ((Mathf.Round(carouselPivot.localRotation.eulerAngles.x/(360/HOLDERDAMOUNT)))*(360/HOLDERDAMOUNT)+(360/HOLDERDAMOUNT));
			carouselPivot.localEulerAngles=new Vector3(nextAngle,carouselPivot.localRotation.eulerAngles.y,carouselPivot.localRotation.eulerAngles.z);
		}
		
	}
	
	//Init thumbnail pool
	void initializePool(){
		//thumbnailPoolObject.position=POOLPOSITION;
		thumbnailPool=new List<Thumbnail>();
		//If below 7, instantiate multiple thumbnails per vehicle
		for(int i=0;i<thumbnails.Count;i++){
			//Instantiate one thumbnail per item at init and more on the go if necessary
			Thumbnail newThumbnail = instantiateNewThumbnail(i,false);
			thumbnailPool.Add(newThumbnail);
		}	
	}	
	
	//Returns vehicle thumbnail transform to i:th thumbnail
	private Thumbnail getThumbnailFromPool(int i){
		//if thumbnails in pool get one
		if(thumbnailPool!=null && thumbnailPool.Count>0){
			foreach(Thumbnail t in thumbnailPool){
				if(t.index==i){
					Thumbnail temp=t;
					thumbnailPool.Remove(t);
					temp.t.gameObject.SetActive(true);
					return temp;
				}
			}
		}
		//else instantiate new one
		return instantiateNewThumbnail(i,true);
		
	}
	
	private void returnThumbnailToPool(Thumbnail thumb){
		thumbnailPool.Add(thumb);
		thumb.t.parent=thumbnailPoolObject;
		thumb.t.gameObject.SetActive(false);
	}
	
	//Adds thumbnail indexed i to holder h. Thumbnail is taken from the pool.
	private void moveThumbnailToHolder(int i, ThumbnailHolder h){
		Thumbnail th= getThumbnailFromPool(i);
		h.thumbnail=th;
		th.t.rotation=h.holder.rotation;
		if(horizontal) th.t.Rotate(Vector3.forward,90,Space.Self);
		th.t.parent=h.holder;
		th.t.localPosition=Vector3.zero;
		th.t.gameObject.renderer.enabled=true;
	}
	
	//Resolves which thumbnail should be attached to holder given at argument and calls moveThumbnailToHolder 
	public void holderEnteredVisible(ThumbnailHolder h){
		//add holder to visible list 
		visibleHolders.Add(h);
		//first to be added
		if(visibleHolders.Count==1){
			moveThumbnailToHolder(0,h);
		}
		//spawner entered from top
		else if(hasNextHolderThumbnail(h)){
			moveThumbnailToHolder(getPreviousThumbnailIndex(getNextHolder(h).thumbnail.index),h);
		}
		//spawner entered from bottom
		else if(hasPreviousHolderThumbnail(h)){
			moveThumbnailToHolder(getNextThumbnailIndex(getPreviousHolder(h).thumbnail.index),h);
		}
		//direct neighbor holders dont's have thumbnails, search further
		else{
			int killwhile=0;
			int indx=-1;
			ThumbnailHolder nextHolder = getNextHolder(h);
			ThumbnailHolder previousHolder = getPreviousHolder(h);
			bool foundPrevious=false;
			bool foundNext=false;
			//k is the offset from this spawner
			int k=2;
			
			//iterate to both directions and check where the closest thumbnail is found
			while(!foundPrevious && !foundNext && killwhile<HOLDERDAMOUNT){
				nextHolder = getNextHolder(nextHolder);
				previousHolder = getPreviousHolder(previousHolder);
				if(hasNextHolderThumbnail(nextHolder)){
					foundNext=true;
					indx=getNextHolder(nextHolder).thumbnail.index;
				}
				if(hasPreviousHolderThumbnail(previousHolder)){
					foundPrevious=true;
					indx=getPreviousHolder(previousHolder).thumbnail.index;
				}
				k++;
				killwhile++;
			}
			//iterate back
			while(k>0){
				if(foundNext){
					indx=getPreviousThumbnailIndex(indx);
				}
				else if(foundPrevious){
					indx=getNextThumbnailIndex(indx);
				}
				k--;
			}
			moveThumbnailToHolder(indx,h);
		}
	}
	
	public void holderExitedVisible(ThumbnailHolder h){
		if(h.thumbnail==null || h.thumbnail.index==-1){
			Debug.Log("Holder exited visible with error, thumbnail null: "+h.thumbnail==null +", index: "+h.thumbnail.index);
			return;
		}
		visibleHolders.Remove(h);
		returnThumbnailToPool(h.thumbnail);
		h.thumbnail=null;
		
	}
	
	//Returns index of next thumbnail. If i==thumbnails.size returns 0, otherwise returns i+1
	private int getNextThumbnailIndex(int i){
		if(i==-1 || linkedIndexList.Find(i)==null){
			return -1;
		}
		
		if(linkedIndexList.Find(i).Next!=null){
			return linkedIndexList.Find(i).Next.Value;
		}
		else{
			return linkedIndexList.First.Value;
		}
	}	
	
	private int getPreviousThumbnailIndex(int i){
		if(i==-1 || linkedIndexList.Find(i)==null){
			return -1;
		}
		if(linkedIndexList.Find(i).Previous!=null){
			return linkedIndexList.Find(i).Previous.Value;
		}
		else{
			return linkedIndexList.Last.Value;
		}
	}
	
	// True if previous spawner already has thumbnail, i is index of spawner
	private bool hasPreviousHolderThumbnail(ThumbnailHolder h){
		int i=-1;
		for(int j=0;j<holders.Length;j++){
			if(holders[j]==h) i=j;
		}
		if(i==-1){
			return false;
		}
		if(i==0){
			return holders[HOLDERDAMOUNT-1].thumbnail!=null;
		}
		else{
			return holders[i-1].thumbnail!=null;
		}
	}
	
	private bool hasNextHolderThumbnail(ThumbnailHolder h){
		int i=-1;
		for(int j=0;j<holders.Length;j++){
			if(holders[j]==h) i=j;
		}
		if(i==-1){
			return false;
		}	
		if(i==(HOLDERDAMOUNT-1)){
			return holders[0].thumbnail!=null;
		}
		else{
			return holders[i+1].thumbnail!=null;
		}
	}
	
	private int getNextHolderIndex(ThumbnailHolder h){
		int i=-1;
		for(int j=0;j<holders.Length;j++){
			if(holders[j]==h) i=j;
		}
		if(i==-1){
			return -1;
		}
		if(i==HOLDERDAMOUNT-1) return 0;
		else return i+1;
	}
	
	private int getNextHolderIndex(int i){
		if(i==-1){
			return -1;
		}
		if(i==HOLDERDAMOUNT-1) return 0;
		else return i+1;
	}
	
	private int getPreviousHolderIndex(ThumbnailHolder h){
		int i=-1;
		for(int j=0;j<holders.Length;j++){
			if(holders[j]==h) i=j;
		}
		if(i==-1){
			return -1;
		}
		if(i==0) return HOLDERDAMOUNT-1;
		else return i-1;
	}
	
	private int getPreviousHolderIndex(int i){
		if(i==-1){
			return -1;
		}
		if(i==0) return HOLDERDAMOUNT-1;
		else return i-1;
	}
	
	private ThumbnailHolder getNextHolder(ThumbnailHolder h){
		return holders[getNextHolderIndex(h)];
	}
	
	private ThumbnailHolder getPreviousHolder(ThumbnailHolder h){
		return holders[getPreviousHolderIndex(h)];
	}
	
	//Centers carousel instantly so that thumbnail with index i is at the center
	private void centerToThumbnail(int i){
		int k=0;
		updateSelectedThumbnailIndex();
		while(i!=-1 && selectedThumbnailIndex!=i && k<thumbnails.Count+1){			
			rotateToNext();
			updateVisibleThumbnails();
			updateSelectedThumbnailIndex();
			
			//print ("selected: "+selectedThumbnailIndex);
			k++;
		}
	}
	
	//Takes thumbnail index as argument i. If enabled is true, trasform is returned with enabled colliders and renderers and vice versa.
	private Thumbnail instantiateNewThumbnail(int i, bool enabled){
		//Instantiate new transform
		Transform newThumbnailTransform = GameObject.CreatePrimitive(PrimitiveType.Cube).transform;
		if(newThumbnailTransform.collider) Destroy(newThumbnailTransform.collider);
		newThumbnailTransform.name="Pooled thumbnail";
		newThumbnailTransform.parent=thumbnailPoolObject;
		newThumbnailTransform.localScale= new Vector3(thumbnailSize.x,thumbnailSize.y,0.001f);
		//Material
		newThumbnailTransform.renderer.material= materials[i];
		if(!enabled){
			foreach(Renderer r in newThumbnailTransform.GetComponents<Renderer>()) r.enabled=false;
		}
		//Create thumbnail object
		Thumbnail newThumb= new Thumbnail(newThumbnailTransform,i);
		allThumbnails.Add(newThumb);		
		//If there's attachements in attachedObjects, attach them
		if(attachedObjects[i].Count>0){
			for(int j=0;j<attachedObjects[i].Count;j++){
				//print ("i"+i+"j"+j);
				addObjectToOneThumbnail(newThumb,attachedObjects[i][j],attachedOffset[i][j],attachedSize[i][j]);
			}
		}
		
		
		return newThumb;
	}
	
	public void changeSelectedIndex(int i){
		selectedThumbnailIndex=i;
		if(i!=-1)lastIndex=i;
	}
	
	//Returns true if middle thumbnail was clicked. Sets click to false so mehtod can be called from OnGUI() which is asyncronous from Update()
	public bool clicked(){
		bool temp=click;
		if(click) click=false;
		return temp;
	}
	
	
	
	//*********** ATTACHMENT FUNCTIONS***********/
	
	//NOTE: when object is attached to thumbnail, the control over that object is taken over and the object should not be referenced again outside this class
	
	//private helper functions
	private void addObjectToOneThumbnail(Thumbnail th,Transform t,Vector3 offset,Vector3 size){	
		t.gameObject.SetActive(true);
		Transform newAttachment = GameObject.Instantiate(t,Vector3.zero,Quaternion.identity) as Transform;
		newAttachment.parent=th.t;
		newAttachment.localScale=size;
		newAttachment.localEulerAngles=Vector3.zero;
		newAttachment.localPosition=new Vector3(offset.x,offset.y,offset.z/(th.t.localScale.z));
		t.gameObject.SetActive(false);
			
		
	}	
	
	private void addAttachment(int i, Transform t,Vector3 offset,Vector3 size){		
		attachedObjects[i].Add(t);
		attachedOffset[i].Add(offset);
		attachedSize[i].Add(size);
		t.gameObject.SetActive(false);
	}
	
	//Adds transform t to thumbnails indexed by i,  no need for pooling because pooling is managed via thumbnails that these objects are attached to
	public void addObjectToThumbnail(int i,Transform t,Vector3 offset,Vector3 size){
		if(i>=thumbnails.Count || i<0)Debug.Log("Couldn't attach object, invalid thumbnail index");
		else{
			addAttachment(i,t,offset,size);
			foreach(Thumbnail th in allThumbnails){
				if(th.index==i){
					addObjectToOneThumbnail(th,t,offset,size);
				}
			}
		}
	}
	
	//Adds transform t to all thumbnails
	public void addObjectToThumbnail(Transform t,Vector3 offset,Vector3 size){
		for(int i=0;i<attachedObjects.Count;i++){
			addAttachment(i,t,offset,size);
		}
		foreach(Thumbnail th in allThumbnails){
			addObjectToOneThumbnail(th,t,offset,size);
		}
	}
	
	
	IEnumerator setHiddenAtEndOfFrame(bool b){
		yield return new WaitForEndOfFrame();
		hidden=b;
	}
	
	/************NESTED CLASSES************/
	//Thumbnails transform is parented to ThumbnailHolder's transform
	public class ThumbnailHolder{
		
		//Thumbnail reference, null if holder is not listed visible
		public Thumbnail thumbnail;
		//Transform which acts as a parent to thumbnails transform
		public Transform holder;
		
		
		
		public ThumbnailHolder(Transform t, Thumbnail th){
			holder=t;
			thumbnail=th;
		}
		
	}
	
	//Nested class presenting thumbnail which has thumbnail as Transform and index as int.
	public class Thumbnail{
		//thumbnails index
		public int index;
		//thumbnails transform
		public Transform t;
		
		public Thumbnail(Transform t, int i){
			this.t=t;
			index=i;
		}
	}
	
	
}
