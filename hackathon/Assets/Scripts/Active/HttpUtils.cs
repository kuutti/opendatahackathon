﻿using UnityEngine;
using System.Collections;
using System.Text;
using System.Net;
using System.IO;
using System;

public static class HttpUtils {

	public static string Encode(string location) {
		ASCIIEncoding ascii = new ASCIIEncoding();
		byte[] bytes = ascii.GetBytes(location);
		//ASCIIEncoding.Convert(Encoding.Unicode, Encoding.ASCII, bytes);
		string result = Encoding.ASCII.GetString(bytes);
		//return Encoding.ASCII(location);
		return result;
		//new Cache();
	}
	
	
}

public class Request {
	private bool isDone_m = false;
	public bool isDone {
		get { return isDone_m; }
	}
	string request;
	string urlReq;
	string exception_m = null;
	public string exception {
		get { return exception_m; }
	}
	byte[] response_m = null;
	public byte[] response {
		get { return response_m; }
	}
	
	public Request(string request, string urlReq, bool b) {
		this.request = request;
		this.urlReq = urlReq;
	}
	
	public void Send(){
		isDone_m = false;
		if (request == "GET") {
			try {
				WebClient client = new WebClient();
				byte[] bytes = client.DownloadData(urlReq); 
				//byte[] bytes = client.DownloadData("http://maps.google.com/maps/api/staticmap?center=Turku&zoom=14&size=2000x2000&maptype=roadmap&sensor=true");
				response_m=bytes;
				/**
				//WWW www = new WWW(url);
				WebClient client = new WebClient();
				// GET /path/script.cgi?field1=value1&field2=value2 HTTP/1.0
				//urlReq = "http://maps.google.com/maps/api/staticmap?center=Turku&zoom=14&size=2000x2000&maptype=roadmap&sensor=true";
				Debug.Log("URL REQ: "+urlReq);
				WebRequest req = HttpWebRequest.Create(urlReq);
				req.Credentials = CredentialCache.DefaultCredentials;
				req.Method = "GET";
				req.Headers.Add("user-agent", "PicnicApp");
				//Stream reqStream = request.GetRequestStream ();
				WebResponse resp = req.GetResponse();
				Stream respStream = resp.GetResponseStream();
				Debug.Log("TEST1");
				//StreamReader reader = new StreamReader (respStream);
				//string responseFromServer = reader.ReadToEnd();
				BinaryReader bReader = new BinaryReader(respStream);
				
				//Debug.Log("TEST2 length="+((int)respStream.Length));
				response_m = new byte[10240];
				int id = 0;
				try {
					for (int i=0; i<response_m.Length; i++) {
						response_m[i] = bReader.ReadByte();
						if (response_m[i] != 0) id = i;
						//Debug.Log((char) response_m[i]);
					}
				} catch (Exception) { Debug.Log("End of stream."); }
				//id = (int)(Mathf.Ceil(((float)id)/32.0f)*32.0f);
				byte[] trimmedBytes = new byte[id];
				Debug.Log("TEST2 id="+id);
				for (int i=0; i<id; i++) {
					trimmedBytes[i] = response_m[i];
				}
				Debug.Log("TEST3");
				response_m = trimmedBytes;
				response_m = bReader.ReadBytes(8192);
				Debug.Log("TEST4");
				//reader.Close ();
				bReader.Close();
	            resp.Close();
				Debug.Log("TEST5");
				//response_m = responseFromServer;
				//System.Net.WebClient
				**/
			} catch (Exception e) {
				exception_m = e.ToString();
			}
		}
		isDone_m = true;
	}
}