using UnityEngine;
using System.Collections;

public class GoogleMap : MonoBehaviour
{
	public enum MapType
	{
		RoadMap,
		Satellite,
		Terrain,
		Hybrid
	}
	public bool loadOnStart = true;
	public bool autoLocateCenter = true;
	public GoogleMapLocation centerLocation;
	
	private int opitimateZoom = 13;
	public int zoom = 13;
	public float zoomMovementMultiplier {
		get {
			int zoomDiff = zoom - opitimateZoom;
			if (zoomDiff == 0) return 1;
			else if (zoomDiff<0) {
				return Mathf.Pow(2, zoomDiff);
			}
			else {
				return Mathf.Pow(0.5f, zoomDiff);
			}
		}
	}
	private int minZoom = 12; // 0
	private int maxZoom = 22; // 22
	
	public MapType mapType;
	public int size = 2000; //512;
	public bool doubleResolution = false;
	public GoogleMapMarker[] markers;
	public GoogleMapPath[] paths;
	
	private bool ready_m = false;
	public bool ready {
		get { return ready_m; }
	}
	
	void Start() {
		if(loadOnStart) Refresh();
		renderer.material.mainTexture.wrapMode = TextureWrapMode.Clamp;
	}
	
	public void Refresh() {
		//Debug.Log("Refresh BEGIN");
		ready_m = false;
		if(autoLocateCenter && (markers.Length == 0 && paths.Length == 0)) {
			Debug.LogError("Auto Center will only work if paths or markers are used.");	
		}
		//StartCoroutine(_Refresh());
		RefreshCoroutine();
		ready_m = true;
		//Debug.Log("Refresh END");
	}
	
	//IEnumerator _Refresh ()
	private void RefreshCoroutine()
	{
		var url = "http://maps.googleapis.com/maps/api/staticmap"; // "http://maps.googleapis.com/maps/api/staticmap";
		var qs = "";
		if (!autoLocateCenter) {
			if (centerLocation.address != "")
				qs += "center=" + HttpUtils.Encode (centerLocation.address);
			else {
				qs += "center=" + HttpUtils.Encode (string.Format ("{0},{1}", centerLocation.latitude, centerLocation.longitude));
			}
		
			qs += "&zoom=" + zoom.ToString ();
		}
		qs += "&size=" + HttpUtils.Encode (string.Format ("{0}x{0}", size));
		qs += "&scale=" + (doubleResolution ? "2" : "1");
		qs += "&maptype=" + mapType.ToString ().ToLower ();
		var usingSensor = false;
#if UNITY_IPHONE
		usingSensor = Input.location.isEnabledByUser && Input.location.status == LocationServiceStatus.Running;
#endif
		qs += "&sensor=" + (usingSensor ? "true" : "false");
		
		foreach (var i in markers) {
			qs += "&markers=" + string.Format ("size:{0}|color:{1}|label:{2}", i.size.ToString ().ToLower (), i.color, i.label);
			foreach (var loc in i.locations) {
				if (loc.address != "")
					qs += "|" + HttpUtils.Encode (loc.address);
				else
					qs += "|" + HttpUtils.Encode (string.Format ("{0},{1}", loc.latitude, loc.longitude));
			}
		}
		
		foreach (var i in paths) {
			qs += "&path=" + string.Format ("weight:{0}|color:{1}", i.weight, i.color);
			if(i.fill) qs += "|fillcolor:" + i.fillColor;
			foreach (var loc in i.locations) {
				if (loc.address != "")
					qs += "|" + HttpUtils.Encode (loc.address);
				else
					qs += "|" + HttpUtils.Encode (string.Format ("{0},{1}", loc.latitude, loc.longitude));
			}
		}
		
		var req = new Request ("GET", url + "?" + qs, true);
		
		req.Send ();
		while (!req.isDone);
			//Debug.Log("yield...");
			//yield return null;
		if (req.exception == null) {
			//Debug.Log("EXELENT!");
			var tex = new Texture2D (size, size);
			tex.LoadImage (req.response);
			renderer.material.mainTexture = tex;
			renderer.material.mainTexture.wrapMode = TextureWrapMode.Clamp;
		}
		//else req.exception = null;
	}
	
	public void Move(Vector2 delta) {
		centerLocation.address = "";
		
		centerLocation.latitude+=delta.y; //delta.y;
		centerLocation.longitude+=delta.x; // delta.x;
	}
	
	public bool ZoomIn() {
		zoom++;
		if (zoom>maxZoom) {
			zoom = maxZoom;
			return false;
		}
		else return true;
	}
	
	public bool ZoomOut() {
		zoom--;
		if (zoom<minZoom) {
			zoom = minZoom;
			return false;
		}
		else return true;
	}
	
}

public enum GoogleMapColor
{
	black,
	brown,
	green,
	purple,
	yellow,
	blue,
	gray,
	orange,
	red,
	white
}

[System.Serializable]
public class GoogleMapLocation
{
	public string address;
	private float latitude_m = 60.448f;
	public float latitude {
		get { return latitude_m;}
		set {
			float lat = value;
			lat = ((float)((int) (lat * 100000)))/100000;
			Debug.Log("Old latitude is "+latitude_m+" new latitude is "+lat);
			latitude_m = lat;
		}
	}
	private float longitude_m = 22.264f;
	public float longitude {
		get { return longitude_m;}
		set {
			float lon = value;
			lon = ((float)((int) (lon * 100000)))/100000;
			Debug.Log("Old longitude is "+longitude_m+" new longitude is "+lon);
			longitude_m = lon;
		}
	}
}

[System.Serializable]
public class GoogleMapMarker
{
	public enum GoogleMapMarkerSize
	{
		Tiny,
		Small,
		Mid
	}
	public GoogleMapMarkerSize size;
	public GoogleMapColor color;
	public string label;
	public GoogleMapLocation[] locations;
	
}

[System.Serializable]
public class GoogleMapPath
{
	public int weight = 5;
	public GoogleMapColor color;
	public bool fill = false;
	public GoogleMapColor fillColor;
	public GoogleMapLocation[] locations;	
}