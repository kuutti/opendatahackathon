﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MapMovement : MonoBehaviour {
	private Vector2 firstPos;
	private Vector2 lastPos;
	
	public List<KeyValuePair<int, float>> speedSteps;
	
	private Rect mapLimits = new Rect(-0.05f,-0.05f,0.1f,0.1f); // x,y,width,height
	private GoogleMap map;
	private bool noTouches = true;
	private bool refresh = false;
	
	private Camera cam;
	
	private Vector3 prevMousePos;
	
	private float prevPinchDistance = 0.0f;
	private float deltaPinchDistance = 0.0f;
	private float pinchStepThreshold = 30.0f;
	
	private float textureOffsetMultiplier = 0.00061f;
	private float movementMultiplier = 0.000034f; //0.000001f;
	private float sensitivity = 0.2f;
	
	private List<Touch> trimmedTouches;
	
	public bool pinchTestBool = false; 
	
	// Use this for initialization
	void Start () {
		map = gameObject.transform.GetComponent<GoogleMap>();
		prevMousePos=Vector3.zero;
		cam = GameObject.Find("Main Camera").camera;
		firstPos = new Vector2(-1, -1);
		lastPos = firstPos;
	}
	
	// Update is called once per frame
	void Update () {
		if (pinchTestBool) {
			pinchTestBool=false;
			map.ZoomIn();
			refresh = true;
		}
		//IF MOUSE DOWN
		trimmedTouches=new List<Touch>();
		trimmedTouches.AddRange(Input.touches);
		#if UNITY_STANDALONE || UNITY_EDITOR || UNITY_WEBPLAYER
		if (Input.GetMouseButton(0)) {
			Touch t = new Touch();
			//t.position = Input.mousePosition;
			trimmedTouches.Add(t);
		}
		#endif
		if(trimmedTouches.Count>0) {
			Ray ray;
			#if IPHONE
			ray = cam.ScreenPointToRay(trimmedTouches[0]);
			#endif
			#if UNITY_STANDALONE || UNITY_EDITOR || UNITY_WEBPLAYER
			ray = cam.ScreenPointToRay(Input.mousePosition);
			#endif
			RaycastHit hit;
			if (Physics.Raycast(ray, out hit,  200.0f)) { // Casts the ray and get the first game object hit
				if(hit.transform.gameObject==gameObject) { // is a park?
					// Give park info
					//Debug.Log("TOUCHE(S) count="+(trimmedTouches.Count));
					noTouches = false;
					if (trimmedTouches.Count==1) {
						if (firstPos.x<0 && firstPos.y<0) {
							#if UNITY_STANDALONE || UNITY_EDITOR || UNITY_WEBPLAYER
								firstPos = Input.mousePosition;
								prevMousePos = firstPos;
								//Debug.Log("THIS IS THE SETTER FOR FIRST: "+firstPos);
							#endif
							#if IPHONE
								firstPos = trimmedTouches[0].position;
							#endif
							lastPos = firstPos;
							// is a click?
							/** 
							Ray ray = cam.ScreenPointToRay(trimmedTouches[0]);
							RaycastHit hit;
							if (Physics.Raycast(ray, out hit,  200.0f)) { // Casts the ray and get the first game object hit
								if(hit.transform.gameObject==gameObject) { // is a park?
									// Give park info
								}
							}
							**/
						}
						else {
							#if UNITY_STANDALONE || UNITY_EDITOR || UNITY_WEBPLAYER
								Vector2 deltaVector = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
								deltaVector.x-=prevMousePos.x;
								deltaVector.y-=prevMousePos.y;
								deltaVector = Vector2.Scale(deltaVector, new Vector2(-textureOffsetMultiplier,-textureOffsetMultiplier));
								gameObject.renderer.material.mainTextureOffset += deltaVector;
								//renderer.material.mainTextureOffset.Set(renderer.material.mainTextureOffset.x + deltaVector.x, renderer.material.mainTextureOffset.y + deltaVector.y);  //.material.SetTextureOffset()
								//gameObject.renderer.material.SetTextureOffset("Offset",gameObject.renderer.material.mainTextureOffset);
								//Debug.Log("Texture Offset: " + gameObject.renderer.material.mainTextureOffset.ToString());//.mainTextureOffset.ToString());
								prevMousePos = Input.mousePosition;
								lastPos = Input.mousePosition;
							#endif
							#if IPHONE
								lastPos = trimmedTouches[0].position;
								gameObject.renderer.material.mainTextureOffset += trimmedTouches[0].deltaPosition;  //.material.SetTextureOffset()
							#endif
							//gameObject.renderer.material.mainTextureOffset += trimmedTouches[0].deltaPosition;  //.material.SetTextureOffset()
						}
					}
					if (trimmedTouches.Count==2) {
						firstPos = new Vector2(-1, -1);
						lastPos = firstPos;
						//Vector2 delta = new Vector2(0,0);
						float tempPinchDistance = trimmedTouches[0].position.magnitude - trimmedTouches[1].position.magnitude;
						deltaPinchDistance += tempPinchDistance - prevPinchDistance;
						prevPinchDistance = tempPinchDistance;
						if (deltaPinchDistance<-pinchStepThreshold) {
							deltaPinchDistance+=pinchStepThreshold;
							// zoom map out
							map.ZoomIn();
						}
						else if (deltaPinchDistance>pinchStepThreshold) {
							deltaPinchDistance-=pinchStepThreshold;
							// zoom map in
							map.ZoomOut();
						}
					}
				}
			}
		}
		else { // no touches
			//Debug.Log("NO TOUCHES");
			gameObject.renderer.material.mainTextureOffset = Vector2.zero;
			if (!noTouches) {
				Vector2 delta = new Vector2(firstPos.x-lastPos.x, firstPos.y-lastPos.y);
				//Debug.Log("FIRST: "+firstPos.ToString()+",  SECOND: "+lastPos+"  DELTA: "+delta.ToString());
				map.Move(Vector2.Scale(delta, new Vector2(2*movementMultiplier*map.zoomMovementMultiplier,movementMultiplier*map.zoomMovementMultiplier)));
				
				firstPos = new Vector2(-1.0f,-1.0f);
				noTouches = true;
				refresh = true;
			}
		}
		
		// REFRESH
		if (refresh) {
			Debug.Log("refresh");
			if (map.ready) {
				refresh = false;
				map.Refresh();
			}
		}
	}
}
