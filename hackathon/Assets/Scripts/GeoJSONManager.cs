﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using MiniJSON;



public class GeoJSONManager : MonoBehaviour {
	
	
	private string FOLDERSTRING="/geojson/";
	private string FILEEXTENTSION=".geojson";
	private string BENCHFILENAME="Penkit_epsg4326 (1)";
	private string TRASHFILENAME="Roskikset_epsg4326";
	private string SERVICES="varsinaissuomenpalvelut2013";
	
	private List<GeoJSONObject> jsonObjs;
	
	 // Example usage:
    //
    //  using UnityEngine;
    //  using System.Collections;
    //  using System.Collections.Generic;
    //  using MiniJSON;
    //
    //  public class MiniJSONTest : MonoBehaviour {
    //      void Start () {
    //          var jsonString = "{ \"array\": [1.44,2,3], " +
    //                          "\"object\": {\"key1\":\"value1\", \"key2\":256}, " +
    //                          "\"string\": \"The quick brown fox \\\"jumps\\\" over the lazy dog \", " +
    //                          "\"unicode\": \"\\u3041 Men\u00fa sesi\u00f3n\", " +
    //                          "\"int\": 65536, " +
    //                          "\"float\": 3.1415926, " +
    //                          "\"bool\": true, " +
    //                          "\"null\": null }";
    //
    //          var dict = Json.Deserialize(jsonString) as Dictionary<string,object>;
    //
    //          Debug.Log("deserialized: " + dict.GetType());
    //          Debug.Log("dict['array'][0]: " + ((List<object>) dict["array"])[0]);
    //          Debug.Log("dict['string']: " + (string) dict["string"]);
    //          Debug.Log("dict['float']: " + (double) dict["float"]); // floats come out as doubles
    //          Debug.Log("dict['int']: " + (long) dict["int"]); // ints come out as longs
    //          Debug.Log("dict['unicode']: " + (string) dict["unicode"]);
    //
    //          var str = Json.Serialize(dict);
    //
    //          Debug.Log("serialized: " + str);
    //      }
    //  }
	
	// Use this for initialization
	void Awake () {
		jsonObjs=new List<GeoJSONObject>();
		//print (Application.dataPath+FOLDERSTRING+BENCHFILENAME+FILEEXTENTSION);
		parseGeoJSON(BENCHFILENAME,"bench");
		parseGeoJSON(TRASHFILENAME,"trashcan");
		
		//string bench = System.IO.File.ReadAllText(Application.dataPath+FOLDERSTRING+BENCHFILENAME+FILEEXTENTSION);
		
		
		//var dict = Json.Deserialize(bench) as Dictionary<string,object>;
		//print (dict.Values.Count);
		
		//print (bench);
	}
		
		
	public string getCoordinate(){
		print (jsonObjs.Count);
		return(jsonObjs[0].coordinates[1]+","+jsonObjs[0].coordinates[0]);
	}
	
	public string[] getBenchCoordinates(){
		List<string> list = new List<string>();
		foreach(GeoJSONObject g in jsonObjs){
			if(g.name=="bench") list.Add(g.coordinates[1]+","+g.coordinates[0]);
		}
		return list.ToArray();
	}
	
	public List<double[]> getBenchCoordinatesList(){
		List<double[]> list = new List<double[]>();
		foreach(GeoJSONObject g in jsonObjs){
			if(g.name=="bench") list.Add(new double[] {double.Parse(g.coordinates[1]),double.Parse(g.coordinates[0])});
		}
		return list;
	}
	
	
	public List<double[]> getTrashCoordinatesList(){
		List<double[]> list = new List<double[]>();
		foreach(GeoJSONObject g in jsonObjs){
			if(g.name=="trashcan") list.Add(new double[] {double.Parse(g.coordinates[1]),double.Parse(g.coordinates[0])});
		}
		return list;
	}
	
	
	
	private void parseGeoJSON(string file,string name){
		string[] lines  = System.IO.File.ReadAllLines(Application.dataPath+FOLDERSTRING+file+FILEEXTENTSION);
		int lineIndex=0;
		while(!lines[lineIndex].Contains("features")) lineIndex++; //skip until finds features
		lineIndex++; //skip features line
		while(lines[lineIndex].Length>0){
			if(lines[lineIndex].Length>1){
				//print (lines[lineIndex]);
				char[] chars=lines[lineIndex].ToCharArray();
				GeoJSONObject go = new GeoJSONObject();
				go.name=name;
				jsonObjs.Add(go);
				int charIndex=lines[lineIndex].IndexOf("coordinates",0);
				//print ("line:"+lines[lineIndex]);
				//print ("charIndex:"+charIndex);
				//print ("length:"+chars.Length);
				
				while(chars[charIndex]!='['){
					charIndex++;
				}
				
				int firstCoordStart=charIndex;
				
				while(chars[charIndex]!=','){
					charIndex++;
				}
				
				int firstCoordStop=charIndex+1;
				int secondCoordStart=charIndex;
				
				while(chars[charIndex]!=']'){
					charIndex++;
				}
				
				int secondCoordStop=charIndex;
				//print ("firstcoordstart:"+firstCoordStart+" firstcoordstop:"+firstCoordStop);
				
				//Offsets due to whitespace
				string firstCoordinate= lines[lineIndex].Substring(firstCoordStart+2,firstCoordStop-firstCoordStart-3);
				string secondCoordinate= lines[lineIndex].Substring(secondCoordStart+2,secondCoordStop-secondCoordStart-3);
				//string firstCoordinate=
				//print ("first:" +firstCoordinate+" second:"+secondCoordinate);
				go.coordinates= new string[] {firstCoordinate,secondCoordinate};
				
				//print (charIndex);
				/*for(int i=0;i<chars.Length;i++){
					print (chars[i]);
				}*/
				
			}
			
			lineIndex++;
		}
	}
	
	//use for services, vector3 point data
	private void parseGeoJSON1(string file,string name){
		string[] lines  = System.IO.File.ReadAllLines(Application.dataPath+FOLDERSTRING+file+FILEEXTENTSION);
		int lineIndex=0;
		while(!lines[lineIndex].Contains("features")) lineIndex++; //skip until finds features
		lineIndex++; //skip features line
		while(lines[lineIndex].Length>0){
			if(lines[lineIndex].Length>1){
				//print (lines[lineIndex]);
				char[] chars=lines[lineIndex].ToCharArray();
				GeoJSONObject go = new GeoJSONObject();
				go.name=name;
				jsonObjs.Add(go);
				int charIndex=lines[lineIndex].IndexOf("coordinates",0);
				//print ("line:"+lines[lineIndex]);
				//print ("charIndex:"+charIndex);
				//print ("length:"+chars.Length);
				
				while(chars[charIndex]!='['){
					charIndex++;
				}
				
				int firstCoordStart=charIndex+1;
				
				while(chars[charIndex]!=','){
					charIndex++;
				}
				
				int firstCoordStop=charIndex;
				int secondCoordStart=charIndex+1;
				
				while(chars[charIndex]!=','){
					charIndex++;
				}
				
				int secondCoordStop=charIndex-1;
				//print ("firstcoordstart:"+firstCoordStart+" firstcoordstop:"+firstCoordStop);
				
				
				string firstCoordinate= lines[lineIndex].Substring(firstCoordStart,firstCoordStop-firstCoordStart);
				string secondCoordinate= lines[lineIndex].Substring(secondCoordStart,secondCoordStop-secondCoordStart);
				//string firstCoordinate=
				//print ("first:" +firstCoordinate+" second:"+secondCoordinate);
				go.coordinates= new string[] {firstCoordinate,secondCoordinate};
				
				//print (charIndex);
				/*for(int i=0;i<chars.Length;i++){
					print (chars[i]);
				}*/
				
			}
			
			lineIndex++;
		}
	}
		
	class GeoJSONObject{		
		public string[] coordinates;
		
		
		
		public string name;
		
		public GeoJSONObject(){
			coordinates= new string[2];
		}
	}
		
	
}
