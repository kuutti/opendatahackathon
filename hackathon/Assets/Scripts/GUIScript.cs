﻿using UnityEngine;
using System.Collections;

public class GUIScript : MonoBehaviour {
	
	
	public Rect button1Rect;
	public Rect button2Rect;
	public Rect button3Rect;
	public Rect button4Rect;
	
	
	
	public GUISkin skin;
	private GUIStyle button1Style;
	private GUIStyle button2Style;
	private GUIStyle button3Style;
	private GUIStyle button4Style;
	
	// Use this for initialization
	void Start () {
		button1Style=skin.GetStyle("Button1");
		button2Style=skin.GetStyle("Button2");
		button3Style=skin.GetStyle("Button3");
		button4Style=skin.GetStyle("Button4");
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	
	void OnGUI(){
		if(GUI.Button(button1Rect,GUIContent.none,button1Style)){
		}
		if(GUI.Button(button2Rect,GUIContent.none,button2Style)){
		}
		if(GUI.Button(button3Rect,GUIContent.none,button3Style)){
		}
		if(GUI.Button(button4Rect,GUIContent.none,button4Style)){
		}
	}
	
}
