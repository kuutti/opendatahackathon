﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class StaticMap: MonoBehaviour{ 

//var url = ""; 
	
	
private WWW www;
	
public GeoJSONManager geo;

	
private string basketURL=" http://i.imgur.com/3BVcmPx.png";
//64*64
//private string trashCanURL="http://i.imgur.com/TIO9AXR.png";
//32*32
private string trashCanURL="http://s17.postimg.org/7tashf7ej/garbage_small.png";	
//64*64
//private string benchURL="http://i.imgur.com/HH2Flcn.png";
//32*32
private string benchURL="http://s17.postimg.org/5zyp1m7gb/bench_small.png";
	
	
string BASEURL="http://maps.googleapis.com/maps/api/staticmap?center=";
	
string QUERYINFO="&size=2000x2000&maptype=roadmap&sensor=false";
	
public string ZOOM="&zoom=15";
	
string vartioVuoriCenter="60.448242,22.276072";
string puolalanPuistoCenter="60.453693,22.260564";
string kupittaanPuistoCenter="60.445001,22.293576";
string urheiluPuistoCenter="60.443408,22.263621";
string samppaLinnanPuistoCenter="60.446358,22.267816";
	
double[] vartioVuoriPosition = new double[2]{60.448242d,22.276072d};
double[] puolalanPuistoPosition = new double[2]{60.453693,22.260564};
double[] kupittaanPuistoPosition = new double[2]{60.445001,22.293576};
double[] urheiluPuistoPosition = new double[2]{60.443408,22.263621};
double[] samppaLinnanPuistoPosition = new double[2]{60.446358,22.267816};
	
	
	
float MARKERTHRESHOLD=200;
	
string markerInfo="&markers=color:blue%7Clabel:S%7C60.448141,22.275879";
	

string url;	
	
string customMarker="http://maps.googleapis.com/maps/api/staticmap?center=Turku&zoom=14&size=2000x2000&maptype=roadmap&sensor=false" +
	"&icon:http://chart.apis.google.com/chart?chst=d_map_pin_icon%26chld=cafe%257C996600%7CVartiovuori+Turku";
	
string NY="http://maps.googleapis.com/maps/api/staticmap?size=480x480&markers=icon:http://chart.apis.google.com/chart?chst=d_map_pin_icon%26chld=cafe%257C996600%7C224+West+20th+Street+NY%7C75+9th+Ave+NY%7C700+E+9th+St+NY&sensor=false";

	

	
	//"224+West+20th+Street+NY%7C75+9th+Ave+NY%7C700+E+9th+St+NY"
	
	void Start(){
		//yield www;
		//string coords ="&markers=color:blue%7Clabel:S%7C"+geo.getCoordinate();
		//string coords ="&markers=icon:"+benchURL+"%26chld=bench%257C996600"+geo.getCoordinate();
		
		//toimii!
		string coords ="&markers==size:tiny%7icon:"+benchURL+"%26chld=bench%257C996600"+"%7Clabel:bench%7C"+geo.getCoordinate();
		
		formURLString(0,true,true);
		
		//print (coords);
		//string test="http://maps.google.com/maps/api/staticmap?center="+samppaLinnanPuistoCenter+"&zoom=17&size=2000x2000&maptype=roadmap&sensor=true";
		string test=BASEURL+vartioVuoriCenter+QUERYINFO+ZOOM;
		
		List<double[]> benchCoords= geo.getBenchCoordinatesList();
		//print ("string "+benchCoords[0]+"  "+benchCoords[1]);
		//print ("float "+float.Parse(benchCoords[0])+"  "+float.Parse(benchCoords[1]));
		foreach(double[] f in benchCoords) print("double: "+f[0]+"  "+f[1]);
		
		//print ("distance"+distance(benchCoords[0],benchCoords[1]));
		
		//print ("benches around vartiovuori"+benchesAroundPosition(vartioVuoriPosition).Length);
		
		//print (test);
		StartCoroutine(loadMapFromGoogle(url));
}
	
	
	IEnumerator loadMapFromGoogle(string url){
		www = new WWW(url); 
		while(!www.isDone){
			yield return new WaitForSeconds(0.01f);
		}
		gameObject.renderer.material.mainTexture=www.texture;
	}
	
	void formURLString(int park, bool showBench, bool showGarbage){
		switch(park){
			//vartiovuori
			case 0:
				if(showBench && showGarbage){
					url=BASEURL+vartioVuoriCenter+QUERYINFO+ZOOM+"&markers=icon:"+benchURL+"%26chld=bench%257C996600";
					double[][] bench = benchesAroundPosition(vartioVuoriPosition);
					for(int i =0;i<bench.Length;i++){
						url+="%7Clabel:bench%7C"+bench[i][0].ToString()+","+bench[i][1].ToString();
					}
				
					url+="&markers=icon:"+trashCanURL+"%26chld=trashcan%257C996600";
					double[][] trashcan = trashCanAroundPosition(vartioVuoriPosition);
					for(int i =0;i<trashcan.Length;i++){
						url+="%7Clabel:trashcan%7C"+trashcan[i][0].ToString()+","+trashcan[i][1].ToString();
					}
				
				}
				break;
			case 1:
				break;
			case 2:
				break;
			case 3:
				break;
			case 4:
				break;
			default:
				break;
			
		}
		
		
		/*if(showBench && showGarbage){}
		if(showBench){
			string[] benchCoords= geo.getBenchCoordinates();
			url=BASEURL+vartioVuoriCenter+QUERYINFO+ZOOM+"&markers=icon:"+benchURL+"%26chld=bench%257C996600";
			//print (url);
			for(int i =0;i<10;i++){
				url+="%7Clabel:bench%7C"+benchCoords[i];
			}
			
		}*/
		//print (url);
	}
	
	
	
	double[][] benchesAroundPosition(double[] pos){
		List<double[]> list = new List<double[]>();
		List<double[]> benchCoords= geo.getBenchCoordinatesList();
		foreach(double[] bench in benchCoords){
			if(distance(bench,pos)<MARKERTHRESHOLD) list.Add(bench);
		}
		return list.ToArray();
	}
	
	
	double[][] trashCanAroundPosition(double[] pos){
		List<double[]> list = new List<double[]>();
		List<double[]> trashCoords= geo.getTrashCoordinatesList();
		foreach(double[] trashcan in trashCoords){
			if(distance(trashcan,pos)<MARKERTHRESHOLD) list.Add(trashcan);
		}
		return list.ToArray();
	}
	
	// Haversine distance formula, see http://en.wikipedia.org/wiki/Haversine_formula
	//a=this b=other [0]=lat [1]=lon
	double distance(double[] a, double[] b){
		float R = 6378137;
		float d2r= Mathf.PI/180f;
		float dLat = (float) (b[0]-a[0])*d2r;
		float dLon = (float)(b[1]-a[1])*d2r;
		float lat1=(float)a[0]*d2r;
		float lat2=(float)b[0]*d2r;
		float sin1=Mathf.Sin(dLat/2f);
		float sin2=Mathf.Sin(dLon/2f);
		
		float at = sin1 * sin1 +sin2 * sin2 * Mathf.Cos(lat1) * Mathf.Cos(lat2);
		
		return (double) R * 2 * Mathf.Atan2(Mathf.Sqrt(at),Mathf.Sqrt(1-at));
		
		
		
		
		
		
	
	}
}