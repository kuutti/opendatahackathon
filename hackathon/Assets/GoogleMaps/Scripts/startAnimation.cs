﻿using UnityEngine;
using System.Collections;

public class startAnimation: MonoBehaviour {

	public int fps;
	private float _fps;
	private float timer;
	public Texture[] frames;
	private int currentFrame = -1;
	
	void Start()
	{
		_fps = 1.0f/fps;
		if (frames.Length>0) {
			currentFrame = 0;
			gameObject.renderer.material.mainTexture = frames[0];
		}
	}
	
	void Update()
	{
		if(timer >0.0f)
			timer -= Time.deltaTime;
		if(timer <=0.0f)
			timer = 0.0f;
		if(timer == 0.0f)
			NextFrame();
	}
	
	public void NextFrame()
	{
		if(currentFrame < frames.Length)
			currentFrame++;
		else
			currentFrame = 1;
		gameObject.renderer.material.mainTexture = frames[currentFrame - 1];
		timer = _fps;
	}
}
